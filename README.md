# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0

### How do I get set up? ###

* clone repository
* run npm install in repository location
* run npm start in repository location
* open browser on http://localhost:4200/

### App description ###

* Blog component retreiving full list of posts and create paginated view for them
* Each post component displays title date and content of post
* On right side of view is the view comments button which will reveal comments for post
* Once you click view comments button content of post will be narrowed to create space for comments

### Code description ###

* There is one main module blog.module
* For page layout, pagination and buttons I used bootstrap
* To be sure that content of post, comment will be shown correctly safeHtml pipe was added.
