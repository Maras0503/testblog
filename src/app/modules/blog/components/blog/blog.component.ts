import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { BlogService } from '../../services/blog.service';
import { IPost } from './interfaces/IPost';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  posts: IPost[] | undefined;
  postsToShow: IPost[] | undefined;
  pageSize = 3;
  pageIndex = 0;

  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
    this.blogService.getBlog(
      {
        page: 1,
        per_page: 1,
      }
    ).subscribe(data => {
      this.posts = data;
      this.postsToShow = this.posts?.slice(0, this.pageSize - 1);
    })
  }

  changePage(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.postsToShow = this.posts?.slice(event.pageIndex * this.pageSize, ((event.pageIndex * this.pageSize) + this.pageSize) - 1);
  }
}
