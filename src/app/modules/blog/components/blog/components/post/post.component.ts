import { Component, Input, OnInit } from '@angular/core';
import { IPost } from '../../interfaces/IPost';
import { SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() post!: IPost;
  commentsVisible = false;

  constructor() { }

  ngOnInit(): void {
  }

  showHideComments(): void {
    this.commentsVisible = !this.commentsVisible;
  }
}
