import { Component, Input, OnInit } from '@angular/core';
import { BlogService } from 'src/app/modules/blog/services/blog.service';
import { IComment } from '../../interfaces/IComment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() postId!: number;

  comments: IComment[] | undefined;

  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
    this.blogService.getComments(
      {
        parentId: this.postId,
      }
    ).subscribe(data => {
      this.comments = data;
    });
  }

}
