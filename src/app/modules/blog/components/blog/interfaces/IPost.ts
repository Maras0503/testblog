export interface IPost {
  date: string | null;
  date_gmt: string | null;
  giud: object;
  id: number;
  link: string;
  modified: string;
  modified_gmt: string;
  slug: string;
  status: string;
  type: string;
  password: string;
  permalink_template: string;
  generated_slug: string;
  title: ITitle;
  content: IContent;
  author: number;
  excerpt: object;
  featured_media: number;
  comment_status: string;
  ping_status: string;
  format: string;
  meta: object;
  sticky: boolean;
  template: string;
  categories: string[];
  tags: string[];
}

export interface IContent {
  rendered: string;
  protected: boolean;
}

export interface ITitle {
  rendered: string;
  protected: boolean;
}
