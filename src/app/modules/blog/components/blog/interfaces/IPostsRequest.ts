export class IPostsRequest {
  page?: number;
  per_page?: number;
}
