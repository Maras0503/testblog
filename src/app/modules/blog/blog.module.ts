import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogComponent } from './components/blog/blog.component';
import { BlogService } from './services/blog.service';
import { HttpClientModule } from '@angular/common/http';
import { PostComponent } from './components/blog/components/post/post.component';
import { SafeHtmlPipe } from './pipes/safeHtmlPipe';
import { MatIconModule } from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import { CommentComponent } from './components/blog/components/comment/comment.component';
import { CommentsComponent } from './components/blog/components/comments/comments.component';



@NgModule({
  declarations: [
    BlogComponent,
    PostComponent,
    SafeHtmlPipe,
    CommentsComponent,
    CommentComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatIconModule,
    MatPaginatorModule,
  ],
  providers: [
    BlogService,
  ],
  exports: [
    BlogComponent,
  ],
})
export class BlogModule { }
