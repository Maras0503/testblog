import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IPost } from '../components/blog/interfaces/IPost';
import { IPostsRequest } from '../components/blog/interfaces/IPostsRequest';
import { ICommentsRequest } from '../components/blog/interfaces/ICommentsRequest';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  getBlog(request: IPostsRequest): Observable<IPost[]> {
    return this.http.get<IPost[]>(`http://mansfeld.pl/wp-json/wp/v2/posts`);
  }
  // return this.http.get<IPost[]>(`http://mansfeld.pl/wp-json/wp/v2/posts?page=${request.page}&per_page=${request.per_page}`);

  getComments(request: ICommentsRequest): Observable<IPost[]> {
    return this.http.get<IPost[]>(`http://mansfeld.pl/wp-json/wp/v2/comments?parent=${request.parentId}`);
  }
}
